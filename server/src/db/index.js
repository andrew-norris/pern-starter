const promise = require('bluebird');

const repos = require('./repos');

const initOptions = {
    promiseLib: promise,
    extend(obj, dc) {
        obj.users = new repos.Users(obj, pgp);
    }
};

const config = {
    host: 'db',
    port: '5432',
    database: 'app',
    user: 'postgres',
    password: 'password'
};

const pgp = require('pg-promise')(initOptions);

const db = pgp(config);

module.exports = db;
