const sql = require('../../sql').users;

const cs = {};


function createColumnsets(pgp) {
    if (!cs.insert) {
        const table = new pgp.helpers.TableName({ 
            table: 'users',
            schema: 'public'
        });

        cs.insert = new pgp.helpers.ColumnSet(['name'], { table });
        cs.update = cs.insert.extend(['?id']);
    }
    return cs;
}

class UsersRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        createColumnsets(pgp);
    }

    async create() {
        return this.db.none(sql.create);
    }

    add(values) {
        return this.db.none('INSERT INTO users(name, email, password) VALUES(${name}, ${email}, ${password})', {
            name: values.name,
            email: values.email,
            password: values.password
        });
    }

    remove(id) {
        return this.db.result('DELETE FROM users WHERE id = $1', +id, r => r.rowCount);
    }
    
    findById(id) {
        return this.db.oneOrNone('SELECT * FROM users WHERE id = $1', +id);
    }

    findByName(name) {
        return this.db.oneOrNone('SELECT * FROM users WHERE name = $1', name);
    }

    findByEmail(email) {
        return this.db.oneOrNone('SELECT * FROM users WHERE email = $1', email)
    }

    all() {
        return this.db.any('SELECT * FROM users');
    }
    
}

module.exports = UsersRepository;