upstream client {
  server client:3000;
}

upstream server {
    server server:5000;
}

upstream adminer {
  server adminer:8080;
}

server {
  listen 80;
  server_name app.daisy.ai localhost;

  # To allow POST on static pages
  error_page  405     =200 $uri;

  location / {
      proxy_pass         http://client;
      proxy_set_header   X-Forwarded-For $remote_addr;
      proxy_set_header   Host $http_host;
  }

  location /api/ {
    proxy_pass          http://server;
    proxy_redirect      off;
    proxy_http_version  1.1;
    add_header          'Access-Control-Allow-Origin' '*';
    add_header          'Access-Control-Allow-Credentials' 'true';
    add_header          'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH';
    add_header          'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
    proxy_set_header    Host $http_host;
    proxy_set_header    X-Real-IP $remote_addr;
    proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Host $server_name;
  }

  location /adminer/ {
      proxy_pass         http://adminer;
      proxy_set_header   X-Forwarded-For $remote_addr;
      proxy_set_header   Host $http_host;
  }

  # deny access to .htaccess files, if Apache's document root
  # concurs with nginx's one
  #
  location ~ /\.ht {
    deny all;
  }
}
